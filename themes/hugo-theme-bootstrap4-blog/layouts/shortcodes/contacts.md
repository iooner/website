{{ range $.Site.RegularPages.ByParam "startdate" }}
{{ if or (eq .Type "spaces") (eq .Type "techtuesday") }}

<a href="{{.Permalink}}"><h2>{{.Title}}</h2></a>
<ul>
    <li><strong>Location:</strong> {{.Params.location}}</li>
    <li><strong>Email:</strong> <a href="mailto:{{.Params.contact}}">{{.Params.contact}}</a></li>
    <li><strong>IRC:</strong> {{.Params.irc}}</li>
    <li><strong>Website:</strong> <a href="{{.Params.site}}">{{.Params.site}}</a></li>
</ul>

{{ end }}{{ end }}
